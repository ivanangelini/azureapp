import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import Pedidos from '@/components/pages/Pedidos'
import PedidosNovo from '@/components/pages/PedidosNovo'
import PedidosInterna from '@/components/pages/PedidosInterna'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'Login',
      meta: {
        title: 'Login'
      }
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/pedidos',
      name: 'Pedidos',
      component: Pedidos,
      meta: {
        title: 'Pedidos'
      }
    },
    {
      path: '/pedidos/novo',
      name: 'PedidosNovo',
      component: PedidosNovo,
      meta: {
        title: 'Novo pedido'
      }
    },
    {
      path: '/pedidos/novo/:produtos',
      name: 'PedidosNovoPre',
      component: PedidosNovo,
      meta: {
        title: 'Novo pedido'
      }
    },
    {
      path: '/pedidos/:id',
      name: 'PedidosInterna',
      component: PedidosInterna,
      meta: {
        title: 'Visualizando pedido'
      }
    }
  ]
})
