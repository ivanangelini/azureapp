export default {
  data () {
    return {
      authUser: ''
    }
  },
  methods: {
    getAuthUser () {
      this.authUser = this.$insumosClinica.get('user')
      return this.authUser
    }
  }
}
