import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import Sidebar from '@/components/shared/Sidebar'
import Header from '@/components/shared/Header'
import Login from '@/components/pages/Login'
import Storage from 'vue-ls'
import VModal from 'vue-js-modal'
import VueMoment from 'vue-moment'
import VueLodash from 'vue-lodash'
import Toasted from 'vue-toasted'
import authUser from '@/components/mixins/authUser'

let optionsStorage = {
  namespace: 'insumosClinica_', // key prefix
  name: 'insumosClinica', // name variable Vue.[ls] or this.[$ls],
  storage: 'local' // storage name session, local, memory
}

Vue.component('app-header', Header)
Vue.component('app-sidebar', Sidebar)
Vue.component('app-login', Login)

Vue.use(Toasted, { iconPack: 'fontawesome' })
Vue.use(VueResource)
Vue.use(Storage, optionsStorage)
Vue.use(VModal)
Vue.use(VueMoment)
Vue.use(VueLodash, { name: 'lodash' })
Vue.mixin(authUser)

Vue.filter('formatarPreco', function (value) {
  if (typeof value !== 'number') {
    return value
  }
  let formatter = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL',
    minimumFractionDigits: 2
  })
  return formatter.format(value)
})

Vue.config.productionTip = false

/* eslint-disable no-new */

router.beforeEach((to, from, next) => {
  let isLogged = Vue.insumosClinica.get('user')

  if (to.fullPath === '/') {
    next(true)
  }

  if (to.fullPath === '/' && isLogged) {
    next({ path: '/home' })
  }

  if (isLogged) {
    next(true)
  } else {
    next({ path: '/' })
  }
})

Vue.http.interceptors.push(function (request) {
  const viacep = request.url.includes('viacep.com.br')
  if (this.$route.fullPath !== '/' && !viacep) {
    request.headers.set('Content-Type', 'application/json')
    request.headers.set('Authorization', Vue.insumosClinica.get('user').token)
  }
})

new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
