const baseUrl = `http://localhost:4445/api`

const actions = {
  carregarPedidos: {
    method: 'POST',
    url: `${baseUrl}/clinica_pedido/getAll`
  },
  carregarUmPedido: {
    method: 'GET',
    url: `${baseUrl}/clinica_pedido{/id}`
  },
  enviarPedido: {
    method: 'POST',
    url: `${baseUrl}/clinica_pedido/add`
  },
  carregarProdutos: {
    method: 'GET',
    url: `${baseUrl}/produto`
  },
  carregarProdutoAutocomplete: {
    method: 'POST',
    url: `${baseUrl}/produto/autoComplete`
  }
}

export const apiResource = function (resource) {
  return resource(`${baseUrl}/`, {}, actions)
}
